#!/bin/bash

if (grep connectionLimit /opt/vmware/srm/conf/vmware-dr.xml); then 
   exit 0
else 
   sed -ie '/libsrm-external-api.so/a <connectionLimit>50</connectionLimit>' /opt/vmware/srm/conf/vmware-dr.xml
fi   

systemctl restart srm-server


###############
#sed -ie '/libsrm-external-api.so/a <connectionLimit>50</connectionLimit>' /opt/vmware/srm/conf/vmware-dr.xml && systemctl restart srm-server && systemctl status srm-server -l
