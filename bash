$- prints The current set of options in your current shell.

himBH means following options are enabled:

H - histexpand: when history expansion is enabled
m - monitor: when job control is enabled
h - hashall: Locate and remember (hash) commands as they are looked up for execution
B - braceexpand: when brace expansion is enabled
i - interactive: when current shell is interactive
