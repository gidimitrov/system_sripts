#!/bin/bash


case $1 in 
 -l | --link)
  ln -s /home/gdimitrov/$2 /srv/salt ;;
 -u | --unlink)
   unlink /srv/salt/$2 ;;
 *)   
 echo "Use -l or --link <filename> to link state in /srv/salt/ "
 echo "Use -u or --unlink <filename> to unlink state in /srv/salt/ "
esac